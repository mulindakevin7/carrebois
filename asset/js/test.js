document.getElementById('rectangle-form').addEventListener('submit', function(event) {
    event.preventDefault();
    const width = parseInt(document.getElementById('width').value);
    const height = parseInt(document.getElementById('height').value);

    const container = document.getElementById('rectangle-container');
    container.innerHTML = ''; // Clear previous content
    container.style.width = width + 'px';
    container.style.height = height + 'px';
    container.className = 'rectangle';
    container.style.left = '0px'; // Reset position
    container.style.top = '0px'; // Reset position

    let count120 = 0;
    let count60 = 0;

    // Fill using 120x120 squares first
    for (let y = 0; y < height; y += 120) {
        for (let x = 0; x < width; x += 120) {
            if (x + 120 <= width && y + 120 <= height) {
                createSquare(container, x, y, 120, 'square-120', '120');
                count120++;
            }
        }
    }

    // Fill remaining gaps using 60x60 squares
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if ((x + 60 <= width && y + 60 <= height) && !isCovered(x, y, 60, 60, container)) {
                createSquare(container, x, y, 60, 'square-60', '60');
                count60++;
            }
        }
    }

    let message = `Nombre de carrés :<br><span style="color: #d7c5ac;">Carrés de 120 cm: ${count120}</span>, <span style="color: #baa490;">carrés de 60 cm: ${count60}</span>`;

    // Check for uncovered areas and fill them with red
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if (!isCovered(x, y, 60, 60, container)) {
                createSquare(container, x, y, 60, 'uncovered', '');
            }
        }
    }

    // Calculate additional squares needed
    const areaCovered = count120 * 120 * 120 + count60 * 60 * 60;
    if (areaCovered < width * height) {
        const remainingArea = (width * height) - areaCovered;
        let additionalSquares60 = 0;

        if (remainingArea > 0) {
            const rectCountWidth = Math.ceil(width / 60);
            const rectCountHeight = Math.ceil(height / 60);
            additionalSquares60 = rectCountWidth * rectCountHeight;
        }

        additionalSquares60 -= count120 * 4;
        additionalSquares60 -= count60;

        if (additionalSquares60 > 0) {
            message += `<br><br><span style="color: red;">Il reste de la surface à couvrir et des coupes seront nécessaires.</span><br><span style="color: red;">Nombre de carrés de 60 cm nécessaires en supplément : ${additionalSquares60}</span>`;

// Ajout de la ligne supplémentaire en gras et sur une nouvelle ligne
            const total60 = count60 + additionalSquares60;
            message += `.<br><b style="font-size: 24px;">Il vous faut au final : ${count120} carré de 120cm et ${total60} carrés de 60 cm</b>`;
        }
    }

    document.getElementById('message').innerHTML = message;
});

function createSquare(container, x, y, size, className, text) {
    const square = document.createElement('div');
    square.style.left = x + 'px';
    square.style.top = y + 'px';
    square.style.width = size + 'px';
    square.style.height = size + 'px';
    square.className = className;
    square.textContent = text;
    container.appendChild(square);
}

function isCovered(x, y, width, height, container) {
    const squares = container.children;
    for (let square of squares) {
        const sqX = parseInt(square.style.left);
        const sqY = parseInt(square.style.top);
        const sqSize = parseInt(square.style.width);

        if (x >= sqX && x + width <= sqX + sqSize && y >= sqY && y + height <= sqY + sqSize) {
            return true;
        }
    }
    return false;
}

document.getElementById('background-image').addEventListener('change', function(event) {
    const file = event.target.files[0];
    if (file) {
        const reader = new FileReader();
        reader.onload = function(e) {
            const backgroundContainer = document.getElementById('background-container');
            backgroundContainer.style.backgroundImage = `url(${e.target.result})`;
            backgroundContainer.style.backgroundSize = 'contain'; // Ensures the background image covers the full width
        }
        reader.readAsDataURL(file);
    }
});

// Make the rectangle draggable
let isDragging = false;
let startX, startY, initialX, initialY;

const rectangleContainer = document.getElementById('rectangle-container');
rectangleContainer.addEventListener('mousedown', function(event) {
    isDragging = true;
    startX = event.clientX;
    startY = event.clientY;
    initialX = rectangleContainer.offsetLeft;
    initialY = rectangleContainer.offsetTop;
    rectangleContainer.style.cursor = 'move';
});

document.addEventListener('mousemove', function(event) {
    if (isDragging) {
        const dx = event.clientX - startX;
        const dy = event.clientY - startY;
        rectangleContainer.style.left = (initialX + dx) + 'px';
        rectangleContainer.style.top = (initialY + dy) + 'px';
    }
});

document.addEventListener('mouseup', function() {
    isDragging = false;
    rectangleContainer.style.cursor = 'default';
});

document.getElementById('increase-btn').addEventListener('click', function() {
    scaleRectangle(1.1);
});

document.getElementById('decrease-btn').addEventListener('click', function() {
    scaleRectangle(0.9);
});

function scaleRectangle(scaleFactor) {
    const rectangleContainer = document.getElementById('rectangle-container');
    const currentWidth = rectangleContainer.offsetWidth;
    const currentHeight = rectangleContainer.offsetHeight;

    const newWidth = currentWidth * scaleFactor;
    const newHeight = currentHeight * scaleFactor;

    rectangleContainer.style.width = newWidth + 'px';
    rectangleContainer.style.height = newHeight + 'px';

    const squares = rectangleContainer.querySelectorAll('.square-60, .square-120, .uncovered');
    squares.forEach(square => {
        const originalLeft = parseFloat(square.style.left);
        const originalTop = parseFloat(square.style.top);
        const originalWidth = parseFloat(square.style.width);
        const originalHeight = parseFloat(square.style.height);

        const newLeft = ((originalLeft / currentWidth) * newWidth) + 'px';
        const newTop = ((originalTop / currentHeight) * newHeight) + 'px';
        const newWidthVal = (originalWidth / currentWidth) * newWidth + 'px';
        const newHeightVal = (originalHeight / currentHeight) * newHeight + 'px';

        square.style.left = newLeft;
        square.style.top = newTop;
        square.style.width = newWidthVal;
        square.style.height = newHeightVal;
    });
}
