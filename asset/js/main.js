let isDragging = false;
let initialOffsetX, initialOffsetY;
let additionalSquares60 = 0;
let count60Inside = 0; // Track the count of 60x60 squares inside the rectangle
let selectedTexture = 'chevrons'; // Default texture
let selectedColor = ''; // Track the selected color
let previousState = null; // Variable to store the previous state
let previousMessage = ''; // Variable to store the previous message

const rectangleContainer = document.getElementById('rectangle-container');

const textureDescriptions = {
    chevrons: "Chevrons: A modern geometric pattern for a contemporary touch to your interior.",
    vagues: "Vagues: Wave patterns for a dynamic and fluid look.",
    lignes: "Lignes: Clean lines for a sleek and minimalist appearance."
};

// Function to handle changing the square background image based on selected texture and color
function changeSquareBackground(event) {
    const selectedImageSrc = event.target.src;
    selectedColor = selectedImageSrc;
    updateSquaresBackground();
}

// Add event listeners to thumbnails
function addThumbnailEventListeners() {
    document.querySelectorAll('.thumbnail').forEach(thumbnail => {
        thumbnail.addEventListener('click', changeSquareBackground);
    });
}

// Update background of squares based on selected texture and color
function updateSquaresBackground() {
    const squares = document.querySelectorAll('.rectangle .square-60, .rectangle .square-120');
    squares.forEach(square => {
        if (selectedColor) {
            square.style.backgroundImage = `url(${selectedColor})`;
        }
    });
}

// Function to handle texture selection change
function handleTextureChange(event) {
    selectedTexture = event.target.value;
    updateThumbnails();
    updateTextureDescription();
}

// Update thumbnails based on selected texture
function updateThumbnails() {
    const thumbnailContainer = document.getElementById('thumbnail-container');
    thumbnailContainer.innerHTML = ''; // Clear previous thumbnails

    const images = [
        `asset/images/green-MOTIFS-SIMULATEUR_${selectedTexture}.jpg`,
        `asset/images/dark-MOTIFS-SIMULATEUR_${selectedTexture}.jpg`,
        `asset/images/brown-MOTIFS-SIMULATEUR_${selectedTexture}.jpg`,
        `asset/images/light-MOTIFS-SIMULATEUR_${selectedTexture}.jpg`
    ];

    images.forEach((src, index) => {
        const img = document.createElement('img');
        img.src = src;
        img.alt = `image${index + 1}`;
        img.className = 'thumbnail';
        thumbnailContainer.appendChild(img);
    });

    addThumbnailEventListeners(); // Re-add event listeners to new thumbnails
}

function updateTextureDescription() {
    const descriptionContainer = document.getElementById('texture-description');
    descriptionContainer.textContent = textureDescriptions[selectedTexture] || '';
}

// Add event listener to texture selection
document.getElementById('Textures').addEventListener('change', handleTextureChange);

// Initial call to display thumbnails for the default selected texture
updateThumbnails();
updateTextureDescription();

rectangleContainer.addEventListener('mousedown', function(event) {
    isDragging = true;
    initialOffsetX = event.clientX - rectangleContainer.offsetLeft;
    initialOffsetY = event.clientY - rectangleContainer.offsetTop;
    rectangleContainer.style.cursor = 'move';

    // Prevent text selection during dragging
    event.preventDefault();

    document.addEventListener('mousemove', mouseMoveHandler);
});

document.addEventListener('mouseup', function(event) {
    isDragging = false;
    rectangleContainer.style.cursor = 'default';

    document.removeEventListener('mousemove', mouseMoveHandler);
});

function mouseMoveHandler(event) {
    if (isDragging) {
        const deltaX = event.clientX - initialOffsetX;
        const deltaY = event.clientY - initialOffsetY;
        let newX = rectangleContainer.offsetLeft + deltaX;
        let newY = rectangleContainer.offsetTop + deltaY;

        const maxX = Math.min(document.body.clientWidth - rectangleContainer.offsetWidth, 1200);
        const maxY = document.body.clientHeight - rectangleContainer.offsetHeight;
        newX = Math.min(Math.max(0, newX), maxX);
        newY = Math.min(Math.max(0, newY), maxY);

        rectangleContainer.style.left = newX + 'px';
        rectangleContainer.style.top = newY + 'px';
    }
}

// Event listener for the input to upload an image
document.getElementById('myFile').addEventListener('change', function(event) {
    const fileInput = event.target;
    const file = fileInput.files[0];
    const container = document.getElementById('image-container');

    if (file) {
        const reader = new FileReader();

        reader.onload = function(e) {
            let img = container.querySelector('img');
            if (!img) {
                // Create a new img element if it doesn't exist
                img = document.createElement('img');
                container.appendChild(img);
            }
            // Set or replace the source of the img element
            img.src = e.target.result;
        };

        reader.readAsDataURL(file);
    } else {
        alert('No file chosen');
    }
});

// Initialize the image position and allow resizing
const drag1 = document.getElementById('rectangle-container');
drag1.style.width = '100px';  // Set initial size
drag1.style.height = '100px';
drag1.addEventListener('mousedown', function(e) {
    drag1.style.cursor = 'move';
    const offsetX = e.clientX - drag1.offsetLeft;
    const offsetY = e.clientY - drag1.offsetTop;

    function mouseMoveHandler(e) {
        drag1.style.left = e.clientX - offsetX + 'px';
        drag1.style.top = e.clientY - offsetY + 'px';
    }

    function mouseUpHandler() {
        drag1.style.cursor = 'default';
        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', mouseUpHandler);
    }

    document.addEventListener('mousemove', mouseMoveHandler);
    document.addEventListener('mouseup', mouseUpHandler);
});

// Event listener for the form submission to calculate the number of squares
document.getElementById('rectangle-form').addEventListener('submit', function(event) {
    event.preventDefault();
    const button = document.querySelector('#rectangle-form button[type="submit"]');
    if (button.innerText === "Valider") {
        calculateSquares();
        button.innerText = "Re-initialiser";
    } else {
        resetForm();
        button.innerText = "Valider";
    }
});

function calculateSquares() {
    const width = parseInt(document.getElementById('width').value);
    const height = parseInt(document.getElementById('height').value);

    const container = document.getElementById('rectangle-container');
    container.innerHTML = ''; // Clear previous content
    container.style.width = width + 'px';
    container.style.height = height + 'px';
    container.className = 'rectangle';

    let count120 = 0;
    let count60 = 0;
    count60Inside = 0; // Reset the count

    // Fill using 120x120 squares first
    for (let y = 0; y < height; y += 120) {
        for (let x = 0; x < width; x += 120) {
            if (x + 120 <= width && y + 120 <= height) {
                createSquare(container, x, y, 120, 'square-120', '120');
                count120++;
            }
        }
    }

    // Fill remaining gaps using 60x60 squares
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if ((x + 60 <= width && y + 60 <= height) && !isCovered(x, y, 60, 60, container)) {
                createSquare(container, x, y, 60, 'square-60', '60');
                count60++;
                count60Inside++;
            }
        }
    }

    let message = ``;

    // Check for uncovered areas and fill them with green
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if (!isCovered(x, y, 60, 60, container)) {
                createSquare(container, x, y, 60, 'uncovered', '', true);
            }
        }
    }

    // Calculate additional squares needed
    const areaCovered = count120 * 120 * 120 + count60 * 60 * 60;
    additionalSquares60 = 0; // Reset additional squares

    if (areaCovered < width * height) {
        const remainingArea = (width * height) - areaCovered;
        if (remainingArea > 0) {
            const rectCountWidth = Math.ceil(width / 60);
            const rectCountHeight = Math.ceil(height / 60);
            additionalSquares60 = rectCountWidth * rectCountHeight;
        }

        additionalSquares60 -= count120 * 4;
        additionalSquares60 -= count60;

        if (additionalSquares60 > 0) {
            // Add the additional information in brackets next to the 60x60 square count
            const total60 = count60 + additionalSquares60;
            message += `<br><br>Carrés de 60x60 cm : <b id="hover-message">${total60}</b> <span style="color: #94B5A3;">( dont ${additionalSquares60} à découper)</span>`;
            message += `<br><br> Carrés de 120x120 cm: <b>${count120}</b>`;
        }
    }
    document.getElementById('message').innerHTML = message;

    // Add hover message with details
    const hoverMessageElement = document.getElementById('hover-message');
    if (hoverMessageElement) {
        hoverMessageElement.setAttribute('title', `${count60Inside} inside the rectangle and ${additionalSquares60} from cut`);
    }
}

// Event listener for toggling split/unsplit of squares
document.getElementById('toggle-split').addEventListener('click', function() {
    // Save the current state and message before toggling
    previousState = rectangleContainer.innerHTML;
    previousMessage = document.getElementById('message').innerHTML;

    const container = document.getElementById('rectangle-container');
    container.innerHTML = ''; // Clear previous content

    const width = parseInt(document.getElementById('width').value);
    const height = parseInt(document.getElementById('height').value);
    container.style.width = width + 'px';
    container.style.height = height + 'px';
    container.className = 'rectangle';

    let count60 = 0;
    count60Inside = 0; // Reset the count

    // Fill entire area with 60x60 squares
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if (x + 60 <= width && y + 60 <= height) {
                createSquare(container, x, y, 60, 'square-60', '60');
                count60++;
                count60Inside++;
            }
        }
    }

    // Check for uncovered areas and fill them with green
    for (let y = 0; y < height; y += 60) {
        for (let x = 0; x < width; x += 60) {
            if (!isCovered(x, y, 60, 60, container)) {
                createSquare(container, x, y, 60, 'uncovered', '', true);
            }
        }
    }

    const total60 = count60;
    let additionalMessage = '';

    // Check if additionalSquares60 should be displayed
    if (additionalSquares60 > 0) {
        additionalMessage = ` <span style="color: #94B5A3;">( dont ${additionalSquares60} à découper)</span>`;
    }

    const message = `Carrés de 60x60 cm : <b id="hover-message">${total60 + additionalSquares60}</b>${additionalMessage}<br><br> Carrés de 120x120 cm: <b>0</b>`;
    document.getElementById('message').innerHTML = message;

    // Add hover message with details
    const hoverMessageElement = document.getElementById('hover-message');
    if (hoverMessageElement) {
        hoverMessageElement.setAttribute('title', `${count60Inside} inside the rectangle and ${additionalSquares60} from cut`);
    }
});

// Event listener for the undo button
document.getElementById('undo-split').addEventListener('click', function() {
    if (previousState) {
        rectangleContainer.innerHTML = previousState;
        document.getElementById('message').innerHTML = previousMessage; // Restore the message
        previousState = null; // Clear the saved state
        previousMessage = ''; // Clear the saved message
    }
});

function resetForm() {
    const container = document.getElementById('rectangle-container');
    container.innerHTML = ''; // Clear previous content
    container.style.width = '100px'; // Reset to initial size
    container.style.height = '100px';
    container.className = '';

    const imageContainer = document.getElementById('image-container');
    const img = imageContainer.querySelector('img');
    if (img) {
        imageContainer.removeChild(img);
    }

    document.getElementById('message').innerHTML = '';
    document.querySelector('.split_result').innerHTML = '';

    additionalSquares60 = 0; // Reset additional squares
    count60Inside = 0; // Reset count inside
}

// Function to create a square element
function createSquare(container, x, y, size, className, text, isAdditional = false) {
    const square = document.createElement('div');
    square.style.left = x + 'px';
    square.style.top = y + 'px';
    square.style.width = size + 'px';
    square.style.height = size + 'px';
    square.className = className;
    square.textContent = text;

    if (!isAdditional && selectedColor) {
        square.style.backgroundImage = `url(${selectedColor})`;
    } else if (isAdditional) {
        square.style.backgroundColor = '#94B5A4'; // Original green color
    }

    // Add event listener for double-click to rotate the square
    square.addEventListener('dblclick', function() {
        // Get current rotation value or default to 0
        let rotation = parseInt(square.dataset.rotation) || 0;
        // Increment rotation by 90 degrees clockwise
        rotation += 90;
        // Apply rotation to square
        square.style.transform = `rotate(${rotation}deg)`;
        // Store updated rotation value in dataset
        square.dataset.rotation = rotation;
    });

    container.appendChild(square);
}

function isCovered(x, y, width, height, container) {
    const squares = container.children;
    for (let square of squares) {
        const sqX = parseInt(square.style.left);
        const sqY = parseInt(square.style.top);
        const sqSize = parseInt(square.style.width);

        if (x >= sqX && x + width <= sqX + sqSize && y >= sqY && y + height <= sqY + sqSize) {
            return true;
        }
    }
}

// Increase size of dragged rectangle
function handleScale(action) {
    return (e) => {
        e.preventDefault();
        const container = document.getElementById('rectangle-container');
        const currentScale = parseFloat(container.style.transform.replace('scale(', '').replace(')', '')) || 1;
        const scaleFactor = action === 'add' ? 1.09 : 1 / 1.2;
        const newScale = currentScale * scaleFactor;
        container.style.transform = `scale(${newScale})`;
    };
}
document.getElementById('add-scale').addEventListener('click', handleScale('add'));
document.getElementById('remove-scale').addEventListener('click', handleScale('remove'));

// Event listener for the reset button to clear the split result
document.querySelector('[type="reset"]').addEventListener('click', function() {
    document.querySelector('.split_result').innerHTML = '';
    document.getElementById('message').innerHTML = '';
    additionalSquares60 = 0; // Reset additional squares when the form is reset
});
